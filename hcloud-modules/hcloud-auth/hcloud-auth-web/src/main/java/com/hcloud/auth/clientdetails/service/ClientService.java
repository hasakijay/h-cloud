package com.hcloud.auth.clientdetails.service;

import com.hcloud.auth.clientdetails.entity.ClientEntity;
import com.hcloud.common.crud.service.BaseDataService;

/**
 * @Auther hepangui
 * @Date 2018/10/31
 */
public interface ClientService
        extends BaseDataService<ClientEntity> {

    void modifyPass(String id, String oldPsw, String newPsw);

    void resetPass(String userId);
}
