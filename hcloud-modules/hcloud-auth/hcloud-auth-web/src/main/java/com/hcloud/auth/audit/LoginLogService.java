package com.hcloud.auth.audit;

import com.hcloud.audit.api.bean.LoginLog;
import com.hcloud.audit.api.event.LoginLogEvent;
import com.hcloud.audit.api.util.IPUtils;
import com.hcloud.auth.api.config.LoginType;
import com.hcloud.auth.third.ThirdLoginUtil;
import com.hcloud.common.core.util.SpringUtil;
import eu.bitwalker.useragentutils.UserAgent;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * @Auther hepangui
 * @Date 2018/11/20
 */
@Service
public class LoginLogService {

    @Value("${hcloud.log.login.enable:false}")
    private boolean enableLoginLog;

    private void saveLoginLog(String account, String username, boolean success, String msg) {

        try {
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
            UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeader("user-agent"));
            LoginLog bean = LoginLog.builder().username(username)
                    .account(account).ipAddr(IPUtils.getIpAddr(request))
                    .browser(userAgent.getBrowser().getName())
                    .msg(msg)
                    .device(userAgent.getOperatingSystem().getName())
                    .osName(userAgent.getOperatingSystem().getDeviceType().getName())
                    .success(success ? 1 : 0).build();
            LoginType loginType = ThirdLoginUtil.getLoginType(request);
            switch (loginType) {
                case weixin:
                    bean.setLoginType("微信");
                    break;
                case qq:
                    bean.setLoginType("qq");
                    break;
                case pass:
                    bean.setLoginType("账号");
                    break;
                case gitee:
                    bean.setLoginType("码云");
                    break;
                case mobile:
                    bean.setLoginType("手机");
                    break;
            }
            SpringUtil.publishEvent(new LoginLogEvent(bean));
        } catch (Exception e) {
        }
    }

    public void saveSuccessLog(String account, String username) {
        if(!enableLoginLog){
            return;
        }
        this.saveLoginLog(account, username, true, "");
    }

    public void saveErrorLog(String account, String msg) {
        if(!enableLoginLog){
            return;
        }
        if (account == null) {
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
            String username = request.getParameter("username");
            if (username != null && username.length() > 0) {
                account = username;
            } else {
                String mobile = request.getParameter("mobile");
                if (mobile != null) {
                    account = mobile;
                } else {
                    account = "/";
                }
            }

        }
        this.saveLoginLog(account, "", false, msg);
    }
}
