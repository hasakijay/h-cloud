package com.hcloud.system.user.service;

import com.hcloud.common.crud.service.BaseDataService;
import com.hcloud.common.core.base.User;
import com.hcloud.system.user.entity.UserEntity;

/**
 * @Auther hepangui
 * @Date 2018/10/31
 */
public interface UserService
        extends BaseDataService<UserEntity> {
    UserEntity findByAccount(String account);

    UserEntity findByMobile(String mobile);

    UserEntity findByWeixin(String weixin);

    UserEntity findByQq(String qq);

    void modifyPass(User user, String oldPsw, String newPsw);

    void resetPass(String userId);
}
