package com.hcloud.system.role.service;

import com.hcloud.common.crud.service.BaseDataService;
import com.hcloud.system.role.entity.RoleAuthEntity;
import com.hcloud.system.role.entity.RoleEntity;

import java.util.List;

/**
 * @Auther hepangui
 * @Date 2018/11/5
 */
public interface RoleService extends BaseDataService<RoleEntity> {
    List<RoleAuthEntity> findRoleAuthEntityByRoleId(String roleId);

    void saveAuth(String roleId, String ids);

    /**
     * 删除角色，并删除对应的权限
     *
     * @param id
     */
    void deleteOne(String id);

    List<String> findAuthorityByRoleIds(List<String> roleIds);
}
