package com.hcloud.common.sms.config;

import lombok.Data;

/**
 * @Auther hepangui
 * @Date 2018/12/13
 */
@Data
public class SmsCodeConfig {
    /**
     * 验证码长度
     */
    private int length = 6;

    /**
     * 过期时间
     */
    private int expireIn = 60;
}
