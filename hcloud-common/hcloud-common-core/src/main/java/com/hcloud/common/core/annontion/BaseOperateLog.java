package com.hcloud.common.core.annontion;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * baseController中使用此注解标注操作日志
 * @Auther hepangui
 * @Date 2018/11/20
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface BaseOperateLog {
    String title() default "";
    String type();
}
