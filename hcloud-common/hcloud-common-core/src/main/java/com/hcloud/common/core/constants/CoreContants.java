package com.hcloud.common.core.constants;

/**
 * 核心常量
 * @Auther hepangui
 * @Date 2018/10/28
 */
public interface CoreContants {
    /**
     * 用户已锁定，禁用
     */
    Integer USER_LOCKED = 0;
    /**
     * 用户已激活，正常使用
     */
    Integer UER_NOT_LOCKED = 1;

    /**
     * http编码
     */
    String UTF8 = "UTF-8";

    /**
     * JSON 资源
     */
    String CONTENT_TYPE = "application/json; charset=utf-8";
    String DEFAULT_PASSWORD = "123456";
    String TREE_ROOT = "-1";
}
